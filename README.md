AWS_STS_functions.sh

This set of functions and code manages AWS Security Token Service (STS) variables for use with the CLI tools.

http://docs.aws.amazon.com/cli/latest/reference/sts/index.html

-------

```
#######################################################################################################################
# This file: AWS_STS_functions.sh
#
# Summer 2015 by Al Pacheco and Stefan Wuensch
#
# Usage: This is a shell script that is intended to be sourced in 
# a .bash_profile or other shell init script / file.
# Alternatively, you can include it as needed by running "source ./AWS_STS_functions.sh"
# 
# See also my zsh functions at https://github.com/stefan-wuensch/AWS-goodies/blob/master/AWS_STS_functions.zsh
# 
# Requirements: In order to have these functions work, you must have
# previously set up access keys (aws_access_key_id and aws_secret_access_key).
# Those are used for authentication to AWS IAM to get your username, MFA name, 
# and to generate the session token.
#
# *** NOTE: The session credentials generated by these functions WILL NOT WORK
# if you specify "--profile" in the command-line of an AWS CLI call. 
# These functions set up environment variables with unique session keys,
# which are DIFFERENT keys from what you may have in your ~/.aws/config or ~/.aws/credentials file.
# Future deveopemnt work (as of 2015-08-04) is required to use the files for storage
# in addition to / instead of the environment variables used here.
# 
# Additional info:
# http://blogs.aws.amazon.com/security/post/Tx3D6U6WSFGOK2H/A-New-and-Standardized-Way-to-Manage-Credentials-in-the-AWS-SDKs
# http://docs.aws.amazon.com/cli/latest/topic/config-vars.html
# http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html
# 
# 
# Updates:
# 2016-06-17 	- Updated layout (cosmetic only) -- Stefan
# 		- Added STS expiration countdown in prompt, plus output of expiration time when generated -- Stefan
# 
#######################################################################################################################

```
